#!/usr/bin/env python

import json
import os

opts: dict = json.loads(os.environ['CHECKS'])['build']
print(f'cicd build {opts["id"]} {os.environ["TARGET_BRANCH"]} {os.environ["TARGET_COMMIT"]} >> "$CHECK_RESULTS"')
