#!/usr/bin/env groovy

def call(Map<String, String> args) {
    def cmd = sh(
        script: libraryResource('org/infra/ci/check/buildCmd.py'),
        returnStdout: true
    ).trim()
    return withCredentials([
        string(credentialsId: args.akCredId, variable: 'HUAWEICLOUD_SDK_AK'),
        string(credentialsId: args.skCredId, variable: 'HUAWEICLOUD_SDK_SK'),
    ]) {
        exec(cmd)
    }
}
