#!/usr/bin/env groovy

def call(Map<String, String> args) {
    return withCredentials([string(credentialsId: args.keyCredId, variable: 'CODECHECK_KEY')]) {
        exec 'cicd lint codecheck "$PR_URL" >> "$CHECK_RESULTS"'
    }
}
