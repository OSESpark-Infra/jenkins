#!/usr/bin/env groovy

def call(Map<String, String> args) {
    return withCredentials([string(credentialsId: args.keyCredId, variable: 'CODECHECK_KEY')]) {
        def result = exec('cicd lint codecheck "$PR_URL" ${MAJUN_COMMUNITY:+-p "$MAJUN_COMMUNITY"}', returnStdout: true).trim()
        return readJSON(text: result, returnPojo: true)
    }
}
