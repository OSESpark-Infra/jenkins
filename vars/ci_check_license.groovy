#!/usr/bin/env groovy

def call(Map<String, String> args) {
    def result = exec('cicd license "$PR_URL"', returnStdout: true).trim()
    return readJSON(text: result, returnPojo: true)
}
