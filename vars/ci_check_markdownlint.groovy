#!/usr/bin/env groovy

import groovy.transform.Field

@Field
final BASE_PATH = 'org/infra/ci/checks'

def call(Map<String, String> args) {
    final opts = args.opts
    final files = dir('repo') {
        exec("git diff-tree -r --name-only --no-commit-id origin/${TARGET_BRANCH} HEAD", returnStdout: true)
            .trim()
            .tokenize('\n')
            .findAll {
                it.endsWith('.md')
            }
    }
    final params = [
        configFile: "$WORKSPACE/resources/$BASE_PATH/markdownlint/.markdownlint.yaml",
        config: writeYaml(data: (opts.config ?: [:]), returnText: true),
        files: writeYaml(data: files, returnText: true)
    ]
    final results = [
        result: "$WORKSPACE_TMP/result",
        details: "$WORKSPACE_TMP/details",
        text: "$WORKSPACE_TMP/text"
    ]
    container('deno') {
        withEnv([
            "PARAM_CONFIGFILE=${params.configFile}",
            "PARAM_CONFIG=${params.config}",
            "PARAM_FILES=${params.files}",
            "RESULT_RESULT=${results.result}",
            "RESULT_DETAILS=${results.details}",
            "RESULT_TEXT=${results.text}",
            'STEP_URL=https://raw.gitcode.com/OSESpark-Infra/steps/raw/main/apps/markdownlint'
        ]) {
            dir('repo') {
                exec('deno run -A --import-map="$STEP_URL/import_map.json" "$STEP_URL/main.ts"')
            }
        }
    }
    final result = readFile(file: results.result).trim() == 'true'
    final details = readFile(file: results.details).trim()
    final text = readFile(file: results.text).trim()
    return [result: result, details: details, text: text]
}
