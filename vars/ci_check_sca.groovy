#!/usr/bin/env groovy

def call(Map<String, String> args) {
    return withCredentials([
        string(credentialsId: args.tokenCredId, variable: 'ACCESS_TOKEN'),
        string(credentialsId: args.obsCredId, variable: 'SCA_OBS_KEY'),
        string(credentialsId: args.authCredId, variable: 'SCA_AUTH'),
    ]) {
        exec 'cicd sca "$PR_URL" ./repo >> "$CHECK_RESULTS"'
    }
}
