#!/usr/bin/env groovy

def call(Map<String, String> args) {
    return withCredentials([
        string(credentialsId: args.tokenCredId, variable: 'ACCESS_TOKEN'),
        string(credentialsId: args.appId, variable: 'SCA_APP_ID'),
        string(credentialsId: args.secretKey, variable: 'SCA_SECRET_KEY'),
    ]) {
        def result = exec('cicd sca "$PR_URL" ./repo', returnStdout: true).trim()
        return readJSON(text: result, returnPojo: true)
    }
}
