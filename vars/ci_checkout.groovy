#!/usr/bin/env groovy

import groovy.transform.Field

@Field
final REFS = [
    gitee: 'pull',
    gitcode: 'merge-requests'
]

def call() {
    return checkout(scmGit(
        branches: [[name: 'FETCH_HEAD']],
        userRemoteConfigs: [[
            url: REPO_SSH,
            refspec: "+refs/heads/${BRANCH}:refs/remotes/origin/${BRANCH}",
            credentialsId: "jenkins-ssh.$COMMUNITY"
        ]],
        extensions: [
            cloneOption(depth: 1, honorRefspec: true, noTags: true, shallow: true)
        ]
    ))
}
