#!/usr/bin/env groovy

import groovy.transform.Field

@Field
final SCMS = [
    gitee: 'gitee.com',
    gitcode: 'gitcode.com',
]

@Field
final REFS = [
    gitee: 'pull',
    gitcode: 'merge-requests'
]

def call(String scm, Map<String, String> params) {
    return checkout(scmGit(
        branches: [[name: 'FETCH_HEAD']],
        userRemoteConfigs: [[
            url: "git@${SCMS[scm]}:${params.TARGET_REPO}.git",
            refspec: "+refs/${REFS[scm]}/${params.PULL_NUMBER}/head:refs/remotes/origin/${REFS[scm]}/${params.PULL_NUMBER}/head +refs/heads/${params.TARGET_BRANCH}:refs/remotes/origin/${params.TARGET_BRANCH}",
            credentialsId: "jenkins-ssh-$scm"
        ]],
        extensions: [
            cloneOption(depth: 1, honorRefspec: true, noTags: true, shallow: true)
        ]
    ))
}
