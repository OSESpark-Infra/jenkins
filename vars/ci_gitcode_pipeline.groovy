#!/usr/bin/env groovy

def call(Map args) {
    def (owner, repo) = TARGET_REPO.split('/')
    def id = readJSON(text: api("/groups/$owner/projects?search=$repo"), returnPojo: true).find({
        it.path_with_namespace == TARGET_REPO
    }).id
    def data = [
        merge_request_iid: PULL_NUMBER,
        ref: SOURCE_BRANCH,
        stage: STAGE_NAME,
        build_id: BUILD_ID,
        name: STAGE_NAME
    ] + args
    def result = args.results?.find {
        it.name == STAGE_NAME
    }
    if (result) {
        data['state'] = result.result ? 'success' : 'failed'
        data['description'] = result.details ?: result.result ? '' : 'pipeline failed'
        if (result.details.startsWith('https://')) {
            data['target_url'] = result.details
        }
    }
    data.remove('results')
    api("/projects/$id/statuses/$SOURCE_COMMIT", method: 'POST', data: writeJSON(json: data, returnText: true))
}

def api(Map args = [:], String endpoint) {
    final API = 'https://api.gitcode.com/api/v4'
    def data = args.data?.trim() ?: ''
    def method = args.method ?: 'GET'
    return withCredentials([string(credentialsId: 'jenkins-gitcode-token', variable: 'ACCESS_TOKEN')]) {
        return withEnv([
            "METHOD=$method",
            "URL=$API$endpoint",
            "DATA=$data"
        ]) {
            return sh(script: '''#!/bin/bash
retry_codes=(504)
sep='\r\n\r\n'
while :; do
    out=$(curl -kifsSL -X "$METHOD" "$URL" \
        -H 'content-type: application/json' \
        -H "private-token: $ACCESS_TOKEN" \
        -d "$DATA")
    result=$?
    if [ "$result" == 0 ]; then
        body="${out##*"$sep"}"
        cat <<< "$body"
        break
    else
        heads="${out%"$sep"*}"
        heads="${heads##*"$sep"}"
        cat <<< "$heads" >&2
        status=$(head -n1 <<< "$heads" | awk '{print $2}')
        if [[ " ${retry_codes[*]} " != *" $status "* ]]; then
            exit "$result"
        fi
    fi
done
''', returnStdout: true).trim()
        }
    }
}
