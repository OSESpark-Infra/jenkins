#!/usr/bin/env groovy

import groovy.transform.Field

@Field
final CONCLUSION = [
    FAILURE: 'failure',
    ABORTED: 'cancelled'
]

def call(Map args = [:]) {
    def id = args.id
    def data = [
        name: args.name ?: STAGE_NAME,
        head_sha: SOURCE_COMMIT
    ]
    if (id) {
        // update
        data << [
            status: 'completed',
            conclusion: CONCLUSION[currentBuild.currentResult] ?: args.result ? 'success' : 'failure',
            output: [
                title: STAGE_NAME,
                summary: args.details,
                text: args.text
            ]
        ]
        return readJSON(text: api("/repos/$TARGET_REPO/check-runs/$id", method: 'PATCH', data: writeJSON(json: data, returnText: true)), returnPojo: true)
    } else {
        // create
        data << [
            status: 'in_progress'
        ]
        return readJSON(text: api("/repos/$TARGET_REPO/check-runs", method: 'POST', data: writeJSON(json: data, returnText: true)), returnPojo: true)
    }
}

def api(Map args = [:], String endpoint) {
    final API = 'https://gitee.com/api/v5'
    def data = args.data?.trim() ?: ''
    def method = args.method ?: 'GET'
    return withCredentials([string(credentialsId: "jenkins-token.$COMMUNITY", variable: 'ACCESS_TOKEN')]) {
        return withEnv([
            "METHOD=$method",
            "URL=$API$endpoint",
            "DATA=$data"
        ]) {
            return sh(script: '''#!/bin/bash
retry_codes=(504)
sep='\r\n\r\n'
while :; do
    out=$(curl -kifsSL -X "$METHOD" "$URL" \
        -H 'content-type: application/json' \
        -H "private-token: $ACCESS_TOKEN" \
        -d "$DATA")
    result=$?
    if [ "$result" == 0 ]; then
        body="${out##*"$sep"}"
        cat <<< "$body"
        break
    else
        heads="${out%"$sep"*}"
        heads="${heads##*"$sep"}"
        cat <<< "$heads" >&2
        status=$(head -n1 <<< "$heads" | awk '{print $2}')
        if [[ " ${retry_codes[*]} " != *" $status "* ]]; then
            exit "$result"
        fi
    fi
done
''', returnStdout: true).trim()
        }
    }
}
