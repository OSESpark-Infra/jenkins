#!/usr/bin/env groovy

def call(String scm, Map<String, String> params) {
    exec 'env'
    return writeJSON(
        json: [
            name: STAGE_NAME,
            result: false,
            details: 'pipeline failed'
        ],
        file: "$CHECK_RESULTS/$STAGE_NAME"
    )
}
