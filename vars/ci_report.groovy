#!/usr/bin/env groovy

def call() {
    return sh('''
#!/bin/bash -eu
truncate -s -1 "$CHECK_RESULTS"
curl -vfL http://ci-bot:8000/report -X POST -H 'content-type:application/json' -d @- <<EOF
{
    "repo": "$TARGET_REPO",
    "number": "$PULL_NUMBER",
    "checks": [$(tr '\\n' ',' < "$CHECK_RESULTS")]
}
EOF
''')
}
