#!/usr/bin/env groovy

def call(List<Map> checkResults) {
    def body = [
        repo: TARGET_REPO,
        number: PULL_NUMBER,
        checks: checkResults
    ]
    return exec("""
curl -vfL http://ci-bot.infra:8000/report -X POST -H 'content-type:application/json' -d @- <<'EOF'
${writeJSON(json: body, returnText: true)}
EOF
""")
}
