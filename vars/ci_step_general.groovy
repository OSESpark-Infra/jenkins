#!/usr/bin/env groovy

import groovy.text.GStringTemplateEngine

def call(Map<String, String> args) {
    final initializer = args.initializer ?: {}
    final finalizer = args.finalizer ?: { Map _ -> _.data }
    final opts = args.opts
    final matrix = opts.matrix
    if (!matrix) {
        final ctx = initializer()
        return finalizer(ctx: ctx, data: ci_step_generalImpl(opts))
    }
    opts.remove('matrix')
    final engine = new GStringTemplateEngine()
    final tmpl = engine.createTemplate(writeYaml(data: opts, returnText: true))
    parallel matrix.inject([[:]]) { acc, k, varr ->
        varr.collectMany { v ->
            acc.collect { d -> d << [[k]: v] }
        }
    }.collectEntries { bindings ->
        def suffix = bindings.values().join('/')
        ["$STAGE_NAME-$suffix", {
            def ctx = initializer()
            finalizer(ctx: ctx, data: ci_step_generalImpl(readYaml(text: tmpl.make(bindings).toString())))
        }]
    }
}
