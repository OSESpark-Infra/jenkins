#!/usr/bin/env groovy

import groovy.transform.Field

@Field
final BASE_TMPL = libraryResource('org/infra/ci/agents/base.yaml')

def call(Map<String, String> args) {
    final workspace = args.workspace ?: '.'
    final agent = parseTmpl(args ?: [:])
    return script {
        podTemplate(
            yaml: agent,
            workspaceVolume: persistentVolumeClaimWorkspaceVolume(
                claimName: "pvc-workspace-$POD_NAME"
            ),
            idleMinutes: 1
        ) {
            node(POD_LABEL) {
                container('build') {
                    println("POD_LABEL = $POD_LABEL")
                    dir(workspace) {
                        final logFile = "${pwd(tmp: true)}/build_log".toString()
                        final status = exec("""
{ set +x; } 2> /dev/null
touch '$logFile'
set +e -x
(set -e; ${args.script}) > >(tee -ia '$logFile') 2> >(tee -ia '$logFile' >&2)
status=\$?
sync -f '$logFile'
exit \$status
""", shell: args.shell ?: '/bin/bash', returnStatus: true)
                        return [success: status == 0, log: readFile(file: logFile).trim()]
                    }
                }
            }
        }
    }
}

def parseTmpl(Map args) {
    def tmpl = readYaml(text: BASE_TMPL)
    tmpl.metadata.annotations << (args.annotations ?: [:])
    tmpl.spec.nodeSelector << (args.nodeSelector ?: [:])
    def container = tmpl.spec.containers[0]
    container.image = args.image ?: container.image
    container.imagePullPolicy = args.imagePullPolicy ?: container.imagePullPolicy
    container.command = args.command ?: container.command
    container.args = args.args ?: container.args
    container.tty = args.tty ?: container.tty
    container.env.addAll((args.envs ?: [:]).collect { key, value ->
        [name: key, value: value]
    })
    container.resources.requests << (args.resources?.requests ?: [:])
    container.resources.limits << (args.resources?.limits ?: [:])
    tmpl.spec.volumes.addAll(args.volumes ?: [])
    container.volumeMounts.addAll(args.volumeMounts ?: [])
    // security context
    def security = args.security
    if (security) {
        if (security.runAs) {
            tmpl.spec.securityContext << security.runAs.collectEntries { key, value ->
                ['runAs' + key.capitalize(), value]
            }
        }
        if (security.group) {
            tmpl.spec.securityContext.fsGroup = security.group
        }
        if (security.groups) {
            tmpl.spec.securityContext.supplementalGroups.addAll(security.groups)
        }
        if (security.capabilities) {
            container.securityContext.capabilities = security.capabilities
        }
        def appArmor = security.appArmor
        if (appArmor) {
            def anno
            switch (appArmor.type) {
                case 'Unconfined':
                    anno = 'unconfined'
                    break
                case 'RuntimeDefault':
                    anno = 'runtime/default'
                    break
                case 'Localhost':
                    anno = 'localhost/' + appArmor.profile
                    break
            }
            if (anno) {
                tmpl.spec.annotations << ['container.apparmor.security.beta.kubernetes.io/build': anno]
            }
        }
        def seccomp = security.seccomp
        if (seccomp) {
            tmpl.spec.securityContext.seccompProfile = [type: seccomp.type]
            if (seccomp.profile) {
                tmpl.spec.securityContext.seccompProfile << [localhostProfile: seccomp.profile]
            }
        }
    }
    // docker config
    def dockerConfig = args.dockerConfig
    if (dockerConfig) {
        tmpl.spec.volumes << [
            name: dockerConfig.name ?: 'docker-config',
            secret: [
                secretName: dockerConfig.secret ?: 'default-secret',
                items: [[
                    key: dockerConfig.key ?: '.dockerconfigjson',
                    path: dockerConfig.path ?: 'config.json'
                ]]
            ]
        ]
        container.volumeMounts << [
            name: dockerConfig.name ?: 'docker-config',
            mountPath: dockerConfig.mountPath ?: '/root/.docker'
        ]
    }
    return writeYaml(data: tmpl, returnText: true)
}
