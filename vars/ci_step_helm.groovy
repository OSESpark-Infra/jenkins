#!/usr/bin/env groovy

def call(Map<String, String> args) {
    final opts = args.opts
    ci_step_general(
        opts: [
            image: 'cgr.dev/chainguard/helm:latest-dev',
            command: '/bin/bash',
            security: [
                runAs: [
                    user: 65532,
                    group: 65532
                ],
                group: 65532
            ],
            workspace: 'repo',
            script: """
helm template '${params.TARGET_REPO ?: params.REPO}' '${opts.path}' --debug
if [ -n '${params.PULL_NUMBER ?: ''}' ]; then
    helm push "\$(helm package '${opts.path}' --version '${opts.version ?: 'latest'}')" '${opts.registry}/${opts.repo}'
fi
""".trim()
        ],
        finalizer: {
            [name: env.STAGE_NAME, result: it.data.success, text: """
```log
${it.data.log}
```
""".trim()]
        }
    )
}
