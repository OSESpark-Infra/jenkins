#!/usr/bin/env groovy

import groovy.transform.Field

@Field
final BASE_PATH = 'org/infra/ci/agents'

@Field
final BASE_TMPL = libraryResource("$BASE_PATH/base.yaml")

@Field
final CMD = [
    shell: this.&cmd_shell,
    ko: this.&cmd_ko,
    buildkit: this.&cmd_buildkit
]

def call(Map<String, String> args) {
    final opts = args.opts
    final buildType = opts.type
    if (buildType == 'codearts') {
        return withCredentials([
            string(credentialsId: args.akCredId, variable: 'HUAWEICLOUD_SDK_AK'),
            string(credentialsId: args.skCredId, variable: 'HUAWEICLOUD_SDK_SK'),
        ]) {
            final jobId = opts.id
            final result = exec(
                "cicd build $jobId $TARGET_BRANCH $TARGET_COMMIT $PULL_NUMBER",
                returnStdout: true
            ).trim()
            return readJSON(text: result, returnPojo: true)
        }
    }
    final confPath = "$BASE_PATH/conf/${buildType}.yaml"
    final conf = fileExists("resources/$confPath") ? readYaml(text: libraryResource(confPath)) : [:]
    final agent = parseTmpl(deepMerge(conf, opts.podOptions ?: [:]))
    if (!agent || !CMD[buildType]) {
        return [name: 'build', result: false, details: "build type `$buildType` not supported"]
    }
    return script {
        podTemplate(
            yaml: agent,
            workspaceVolume: persistentVolumeClaimWorkspaceVolume(
                claimName: "pvc-workspace-$POD_NAME"
            ),
            idleMinutes: 1
        ) {
            node(POD_LABEL) {
                container('build') {
                    println("POD_LABEL = $POD_LABEL")
                    dir('repo') {
                        final cmd = CMD[buildType](opts)
                        final logFile = "${pwd(tmp: true)}/build_log".toString()
                        final result = exec("""
{ set +x; } 2> /dev/null
touch $logFile
set +e -x
{ ${cmd.content}; } > >(tee -ia '$logFile') 2> >(tee -ia '$logFile' >&2)
status=\$?
sync -f '$logFile'
exit \$status
""", shell: cmd.shell, returnStatus: true) == 0
                        final log = truncateLog(readFile(file: logFile).trim())
                        final details = """
<details>
  <summary>Build job ${result ? 'success' : 'failed'}</summary>

```log
$log
```
</details>""".trim()
                        return [name: 'build', result: result, details: details]
                    }
                }
            }
        }
    }
}

def parseTmpl(Map args) {
    if (!args) {
        return null
    }
    def tmpl = readYaml(text: BASE_TMPL)
    tmpl.metadata.annotations << (args.annotations ?: [:])
    def container = tmpl.spec.containers[0]
    container.image = args.image ?: container.image
    container.imagePullPolicy = args.imagePullPolicy ?: container.imagePullPolicy
    container.command = args.command ?: container.command
    container.args = args.args ?: container.args
    container.tty = args.tty ?: container.tty
    container.env.addAll((args.envs ?: [:]).collect { key, value ->
        [name: key, value: value]
    })
    container.resources.requests << (args.resources?.requests ?: [:])
    container.resources.limits << (args.resources?.limits ?: [:])
    tmpl.spec.volumes.addAll(args.volumes ?: [])
    container.volumeMounts.addAll(args.volumeMounts ?: [])
    // security context
    def security = args.security
    if (security) {
        if (security.runAs) {
            tmpl.spec.securityContext << security.runAs.collectEntries { key, value ->
                ['runAs' + key.capitalize(), value]
            }
        }
        if (security.group) {
            tmpl.spec.securityContext.fsGroup = security.group
        }
        if (security.groups) {
            tmpl.spec.securityContext.supplementalGroups.addAll(security.groups)
        }
        def appArmor = security.appArmor
        if (appArmor) {
            def anno
            switch (appArmor.type) {
                case 'Unconfined':
                    anno = 'unconfined'
                    break
                case 'RuntimeDefault':
                    anno = 'runtime/default'
                    break
                case 'Localhost':
                    anno = 'localhost/' + appArmor.profile
                    break
            }
            if (anno) {
                tmpl.spec.annotations << ['container.apparmor.security.beta.kubernetes.io/build': anno]
            }
        }
        def seccomp = security.seccomp
        if (seccomp) {
            tmpl.spec.securityContext.seccompProfile = [type: seccomp.type]
            if (seccomp.profile) {
                tmpl.spec.securityContext.seccompProfile << [localhostProfile: seccomp.profile]
            }
        }
    }
    // docker config
    def dockerConfig = args.dockerConfig
    if (dockerConfig) {
        tmpl.spec.volumes << [
            name: dockerConfig.name ?: 'docker-config',
            secret: [
                secretName: dockerConfig.secret ?: 'default-secret',
                items: [[
                    key: dockerConfig.key ?: '.dockerconfigjson',
                    path: dockerConfig.path ?: 'config.json'
                ]]
            ]
        ]
        container.volumeMounts << [
            name: dockerConfig.name ?: 'docker-config',
            mountPath: dockerConfig.mountPath ?: '/root/.docker'
        ]
    }
    return writeYaml(data: tmpl, returnText: true)
}

def deepMerge(Map... maps) {
    def result = [:]
    maps.each { map ->
        map.each { k, v ->
            if (result[k] instanceof List) {
                result[k].addAll(v)
            } else {
                result[k] = result[k] instanceof Map ? deepMerge(result[k], v) : v
            }
        }
    }
    return result
}

def splitPath(String path) {
    final segments = path.tokenize('/')
    return [name: segments.last(), dir: segments.dropRight(1).join('/') ?: '.']
}

def truncateLog(String log) {
    final LIMIT = 48000
    return log.size() <= LIMIT ? log : log[((log.findIndexOf(log.size() - LIMIT) { it == '\n' }) + 1)..-1]
}

def cmd_shell(Map opts) {
    return [shell: opts.shell ?: '/bin/bash', content: opts.script]
}

def cmd_ko(Map opts) {
    final repo = opts.image?.repo ?: 'swr.cn-north-4.myhuaweicloud.com/cangjie'
    final name = opts.image?.name ?: REPO
    final version = opts.image?.version ?: 'latest'
    return [
        shell: '/bin/bash',
        content: """
export KO_DOCKER_REPO=$repo/$name
ko build --bare --tags='$version' --platform=linux/amd64,linux/arm64 '${opts.importPath}'
""".trim().toString()
    ]
}

def cmd_buildkit(Map opts) {
    final context = opts.context ?: '.'
    final dockerfile = splitPath(opts.dockerfile ?: 'Dockerfile')
    final imageName = "${opts.image.repo}/${opts.image.name}:${opts.image.version ?: 'latest'}"
    return [
        shell: '/bin/sh',
        content: """
buildctl-daemonless.sh build \
    --frontend dockerfile.v0 \
    --local context="${context}" \
    --local dockerfile="${dockerfile.dir}" \
    --opt filename="${dockerfile.name}" \
    --opt platform=linux/amd64,linux/arm64 \
    --output type=image,name="${imageName}",push=true
""".trim().toString()
    ]
}
