#!/usr/bin/env groovy

def call(Map args = [:], String script) {
    final shell = args.shell ?: '/bin/bash'
    args['script'] = "#!$shell -xe\nset -o pipefail\n${script.stripIndent().trim()}"
    args.remove('shell')
    return sh(args)
}
